Il progetto mira alla realizzazione di un software di nome guess-who? che emula il celebre gioco Indovina Chi. Il gioco prevede due giocatori: fornito un insieme di personaggi e sceltone uno, scopo di tale gioco è indovinare il personaggio scelto dall’avversario mediante una serie di domande che è possibile effettuare a turni alternati.

