package view.playerview;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import controller.resources.Resources;
import model.attribute.feature.Trait;
import model.question.Question;
import model.question.QuestionFactory;
import utilities.Messages;
import utilities.Utilities;
import view.gamedialog.DialogFactory;
import view.viewposition.PositionManager;

import static java.awt.BorderLayout.*;

/**
 * Implementation of PlayerView. 
 */
public class PlayerViewImpl extends JFrame implements PlayerView {

    /***/
    private static final long serialVersionUID = 1L;
    private static final int GAP = 0;

    private final SelectedPanel selectedPanel;
    private final CharactersPanel charactersPanel;
    private final QuestionsPanel questionsPanel;
    private final ActionListener characterSelector;
    private final ActionListener characterGuesser;
    private final Runnable onClose;

    /**
     * Constructor.
     * @param observer
     *                  the observer
     * @param startingAttempts
     *                  the number of starting attempts
     * @param startingQuestions
     *                  the question about starting character
     */
    public PlayerViewImpl(final PlayerViewObserver observer, final Integer startingAttempts, final Set<Question> startingQuestions) {
        super();
        Utilities.requireNonNull(observer, startingAttempts, startingQuestions);
        selectedPanel = new SelectedPanel(startingAttempts, Resources.getDefaultImage());
        charactersPanel = new CharactersPanel(Resources.getNamesAndImages());
        questionsPanel = new QuestionsPanel(startingQuestions);
        characterSelector = e -> { 
            final String name = ((JButton) e.getSource()).getText();
            charactersPanel.setEnabled(false);
            selectedPanel.setSelected(name, Resources.getCharacterImage(name));
            this.changeActionListener();
            observer.select(name);
        };
        characterGuesser = e -> observer.askOpponent(QuestionFactory.from(Trait.NAME, ((JButton) e.getSource()).getText()));
        final ActionListener questionAsker = e -> observer.askOpponent(questionsPanel.getSelectedQuestion());
        charactersPanel.addActionListener(characterSelector);
        questionsPanel.addActionListener(questionAsker);
        this.setLayout(new BorderLayout(GAP, GAP));
        this.add(NORTH, selectedPanel);
        this.add(CENTER, charactersPanel);
        this.add(SOUTH, questionsPanel);
        this.pack();
        this.setMinimumSize(new Dimension(this.getSize()));
        questionsPanel.setPreferredSize(questionsPanel.getSize());
        this.setLocation(PositionManager.getPoint(this.getSize()));
        onClose = () -> { 
            dispose(); 
            observer.quit(); 
        };
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) throws RuntimeException {
                onClose.run();
            }
        });
        this.setTitle(Messages.TITLE);
        this.setVisible(true);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void setEnabled(final boolean b) {
        Utilities.requireNonNull(b);
        charactersPanel.setEnabled(b);
        questionsPanel.setEnabled(b);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void updateCharacters(final Set<String> remainingNames) {
        charactersPanel.update(remainingNames);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void updateQuestions(final Set<Question> remaining) {
        questionsPanel.update(remaining);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void updateAttempts(final Integer remaining) {
        selectedPanel.setAttempts(remaining);
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean showQuestion(final String question) throws InterruptedException {
        return DialogFactory.showQuestionDialog(this, question);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void showMessage(final String message) throws InterruptedException {
        DialogFactory.showMessageDialog(this, message, true);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void showErrorMessage(final String error) {
        DialogFactory.showErrorDialog(this, error);
        onClose.run();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void close(final String message) {
        Arrays.stream(getWindowListeners()).forEach(l -> removeWindowListener(l)); //disable user to exit
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        try {
            DialogFactory.showMessageDialog(this, message, false, e -> onClose.run());
        } catch (InterruptedException ignored) {
            //notifies the interruption of the closure message (the only case now is the shut down of the application)
            //it is ignored because on the ending of the game there's no possible action ongoing that needs to be interrupted.
        }
    }

    private void changeActionListener() {
        charactersPanel.removeActionListener(characterSelector);
        charactersPanel.addActionListener(characterGuesser);
    }

}
