package view.playerview;

import model.question.Question;

/**
 * Modeling interface for a PlayerView Observer.
 */
public interface PlayerViewObserver {

    /**
     * Allows to select this Player Character.
     * @param name 
     *                  the selected name.
     */
    void select(String name);

    /**
     * Allows to ask a Question to the opponent.
     * @throws IllegalArgumentException 
     *                  in case of null argument
     * @param question 
     *                  the question
     */
    void askOpponent(Question question);

    /**
     * Notifies the Controller that the View has been closed.
     */
    void quit();

}
