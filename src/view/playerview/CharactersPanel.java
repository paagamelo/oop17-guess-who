package view.playerview;

import java.awt.*;
import java.awt.event.*;
import java.util.stream.Collectors;
import java.util.List;
import java.util.*;
import utilities.Utilities;
import javax.swing.*;

/*
 * Panel showing characters images and names.
 */
class CharactersPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private static final int CHARACTERS_PER_ROW = (int) Math.round(Utilities.getScreenRatio() * 5);

    private final List<CharacterButton> characters;

    CharactersPanel(final Map<String, Image> namesAndImages) {
        super();
        Utilities.requireNonNull(namesAndImages);
        this.setLayout(new GridLayout(0, CHARACTERS_PER_ROW));
        characters = namesAndImages.entrySet().stream()
                .map(entry -> new CharacterButton(entry.getKey(), entry.getValue()))
                .peek(comp -> this.add(comp))
                .collect(Collectors.toList());
    }

    public void addActionListener(final ActionListener al) {
        Utilities.requireNonNull(al);
        characters.forEach(c -> c.getButton().addActionListener(al));
    }

    public void removeActionListener(final ActionListener al) {
        Utilities.requireNonNull(al);
        characters.forEach(c -> c.getButton().removeActionListener(al));
    }
    @Override
    public void setEnabled(final boolean b) {
        Utilities.requireNonNull(b);
        characters.forEach(c -> c.getButton().setEnabled(b));
    }

    public void update(final Set<String> remainingNames) {
        Utilities.requireNonNull(remainingNames);
        characters.removeAll(characters.stream()
            .filter(cB -> !remainingNames.contains(cB.getButton().getText()))
            .peek(cB -> cB.disableImage())
            .collect(Collectors.toSet()));
    }

    /*
     * Implements a single character area composed by an image and a button with the character's name
     */
    private class CharacterButton extends JComponent {

        private static final long serialVersionUID = 1L;
        private static final int MANTAIN_ASPECT_RATIO = -1;

        private final int border = (int) Math.round(Utilities.getScreenRatio() * 2);
        private final JButton button;
        private final JLabel imageLabel;
        private boolean isDisabled;

        CharacterButton(final String name, final Image image) {
            super();
            this.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(final ComponentEvent e) {
                    final Dimension componentDimension =  new Dimension(e.getComponent().getWidth() - border, e.getComponent().getHeight() - button.getHeight() - border);
                    final ImageIcon newImage = componentDimension.height - imageLabel.getHeight() > componentDimension.width - imageLabel.getWidth() 
                            ? new ImageIcon(image.getScaledInstance(componentDimension.width, MANTAIN_ASPECT_RATIO, Image.SCALE_SMOOTH)) 
                            : new ImageIcon(image.getScaledInstance(MANTAIN_ASPECT_RATIO, componentDimension.height, Image.SCALE_SMOOTH));
                    if (newImage.getIconWidth() <= componentDimension.width && newImage.getIconHeight() <= componentDimension.height) {
                        imageLabel.setIcon(newImage);
                        if (isDisabled) {
                            imageLabel.setIcon(imageLabel.getDisabledIcon());
                        }
                    }
               }
            });
            button = new JButton(name);
            imageLabel = new JLabel(new ImageIcon(image));
            this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            imageLabel.setAlignmentX(CENTER_ALIGNMENT);
            button.setAlignmentX(CENTER_ALIGNMENT);
            this.add(imageLabel);
            this.add(button);
        }

        public JButton getButton() {
            return button;
        }

        public void disableImage() {
            this.isDisabled = true;
            imageLabel.setIcon(imageLabel.getDisabledIcon());
        }
    }
}
