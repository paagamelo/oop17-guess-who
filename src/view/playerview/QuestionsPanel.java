package view.playerview;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.stream.Collectors;
import javax.swing.*;
import model.attribute.feature.Trait;
import model.question.Question;
import utilities.Utilities;

/*
 * Panel showing possible Questions.
 */
class QuestionsPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private final JButton jAsk = new JButton("ask");
    private final Map<String, TabPanel> questions = new HashMap<>();
    private final JTabbedPane tabbedPane = new JTabbedPane();

    QuestionsPanel(final Set<Question> startingQuestions) {
        super();
        Utilities.requireNonNull(startingQuestions);
        this.setLayout(new BorderLayout());
        Arrays.stream(Trait.values()).filter(t -> !t.equals(Trait.NAME)).forEach(t -> {
            final Set<Question> traitQuestions = filterQuestions(t.toString(), startingQuestions);
            if (!traitQuestions.isEmpty()) {
                final TabPanel tab = new TabPanel(traitQuestions);
                tabbedPane.add(t.toString(), tab);
                questions.put(t.toString(), tab);
            }
        });
        jAsk.setEnabled(false);
        this.add(BorderLayout.CENTER, tabbedPane);
        this.add(BorderLayout.EAST, jAsk);
    }

    public void addActionListener(final ActionListener al) {
        Utilities.requireNonNull(al);
        jAsk.addActionListener(al);
    }
    @Override
    public void setEnabled(final boolean b) {
        Utilities.requireNonNull(b);
        jAsk.setEnabled(b);
    }

    public void update(final Set<Question> remainingQuestions) {
        Utilities.requireNonNull(remainingQuestions);
        final Set<String> removeTrait = new HashSet<>();
        questions.entrySet().forEach(e -> {
            final Set<Question> traitQuestions = filterQuestions(e.getKey(), remainingQuestions);
            if (traitQuestions.isEmpty()) {
                removeTrait.add(e.getKey());
            } else {
                e.getValue().update(traitQuestions);
            }
        });
        removeTrait.stream().forEach(t -> {
            tabbedPane.remove(questions.get(t));
            questions.remove(t);
        });
        if (questions.isEmpty()) {
            jAsk.setVisible(false);
        }
    }

    public Question getSelectedQuestion() { 
        return ((TabPanel) tabbedPane.getSelectedComponent()).getQuestion();
    }

    private Set<Question> filterQuestions(final String trait, final Set<Question> questions) {
        return questions.stream().filter(q -> q.toAttribute().getTrait().toString().equals(trait)).collect(Collectors.toSet());
    }
}
