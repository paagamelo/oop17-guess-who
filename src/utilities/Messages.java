package utilities;

/**
 * Collects application's messages. 
 */
public final class Messages {

    /**
     * View messages.
     */
    /***/
    public static final String TITLE = "Guess Who?";
    /***/
    public static final String PLEASE_SELECT = "Please select your character";
    /***/
    public static final String PLAYER_WON = "Congratulations, you won!";
    /***/
    public static final String PLAYER_LOST = "You lost.. ";
    /***/
    public static final String OPPONENT_CHARACTER_WAS = "Opponent's character was ";
    /***/
    public static final String OPPONENT_QUIT = "Opponent's quit";
    /***/
    public static final String HUMAN_LIED = "You lied";
    /***/
    public static final String YES = "Yes";
    /***/
    public static final String NO = "No"; 

    /**
     * Log messages.
     */
    /***/
    public static final String HAS_SELECTED = "Has selected ";
    /***/
    public static final String EXIT = "Has left the game";
    /***/
    public static final String NULL_ARGUMENT = "Null argument";

    /**
     * Question messages.
     */
    /***/
    public static final String QUESTION_PREFIX = "Does he/she have ";
    /***/
    public static final String CHARACTER_QUESTION_PREFIX = "Is your character ";
    /***/
    public static final String GENDER_QUESTION_PREFIX = "Is it a ";
    /***/
    public static final String QUESTION_SUFFIX = "?";

    private Messages() {
    }

}
