package controller.playercontroller;

import model.ai.Ai;
import model.character.Character;
import java.util.Optional;
import controller.gamecontroller.Controller;
import controller.gameoptions.GameSettings;
import controller.resources.Resources;
import model.player.PlayerBuilder;
import utilities.Utilities;

/**
 * Implementation of a Cpu Player Controller.
 */
public class CpuController extends AbstractController {

    private final Ai ai;
    private final Controller controller;

    /**
     * Constructor.
     * @param controller 
     *              the main Controller of the game.
     */
    public CpuController(final Controller controller) {
        super(new PlayerBuilder().setAttempts(GameSettings.getDifficulty().getAttempts()).setCharacters(Resources.getCharacters()).build());
        Utilities.requireNonNull(controller);
        this.ai = new Ai(GameSettings.getDifficulty().getCpuAbility());
        this.controller = controller;
        getPlayer().select(Utilities.getRandom(getPlayer().getRemaining()));
        controller.selected(this, getPlayer().getSelected().getName());
    }

    /**
     * @inheritDoc
     */
    @Override
    public void play() {
        controller.askOpponent(ai.apply(getPlayer()));
    }

    /**
     * @inheritDoc
     */
    @Override
    public void endGame(final Status status, final Optional<Character> opponentCharacter) {
        Utilities.requireNonNull(status, opponentCharacter);
        controller.quit(this);
    }

}
