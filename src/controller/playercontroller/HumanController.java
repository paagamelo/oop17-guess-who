package controller.playercontroller;

import java.util.Optional;
import java.util.stream.Collectors;
import controller.gamecontroller.Controller;
import controller.gameoptions.Difficulty;
import controller.gameoptions.GameSettings;
import controller.resources.Resources;
import model.character.Character;
import model.player.PlayerBuilder;
import model.question.Question;
import model.question.QuestionFactory;
import utilities.Messages;
import utilities.Utilities;
import view.playerview.PlayerView;
import view.playerview.PlayerViewImpl;
import view.playerview.PlayerViewObserver;

/**
 * Implementation of a Human Player Controller.
 */
public class HumanController extends AbstractController implements PlayerViewObserver {

    private final PlayerView view;
    private final Controller controller;

    /**
     * Constructor.
     * @param controller 
     *              the main controller of the game
     */
    public HumanController(final Controller controller) {
        super(new PlayerBuilder().setAttempts(GameSettings.getDifficulty().getAttempts()).setCharacters(Resources.getCharacters()).build());
        Utilities.requireNonNull(controller);
        this.controller = controller;
        view = new PlayerViewImpl(this, GameSettings.getDifficulty().getAttempts(), 
                QuestionFactory.possibleQuestions(getPlayer()).stream().collect(Collectors.toSet()));
    }

    /**
     * @iheritDoc
     */
    @Override
    public void play() {
        view.setEnabled(true);
    }

    /**
     * @iheritDoc
     */
    @Override
    public boolean askPlayer(final Question question) throws InterruptedException {
        Utilities.requireNonNull(question);
        final boolean answer = super.askPlayer(question);
        if (view.showQuestion(question.toString()) != answer) {
            view.showMessage(Messages.HUMAN_LIED);
        }
        return answer;
    }

    /**
     * @iheritDoc
     */
    @Override
    public void registerAnswer(final Question question, final boolean answer) throws InterruptedException {
        super.registerAnswer(question, answer);
        this.updateView(answer);
    }

    /**
     * @iheritDoc
     */
    @Override
    public void endGame(final Status status, final Optional<Character> opponentCharacter) {
        Utilities.requireNonNull(status, opponentCharacter);
        if (status == Status.OPPONENT_QUIT) {
            view.showErrorMessage(Messages.OPPONENT_QUIT);
        } else {
            view.close(status == Status.WON ? Messages.PLAYER_WON : Messages.PLAYER_LOST + (opponentCharacter.isPresent() 
                    ? Messages.OPPONENT_CHARACTER_WAS + opponentCharacter.get().getName() : ""));
        }
    }

    /**
     * @iheritDoc
     */
    @Override
    public void select(final String name) {
        Utilities.requireNonNull(name);
        view.setEnabled(false);
        getPlayer().select(getPlayer().getRemaining().stream().filter(c -> c.getName().equals(name)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("The name does not match any Character: " + name)));
        controller.selected(this, name);
    }

    /**
     * @iheritDoc
     */
    @Override
    public void askOpponent(final Question question) {
        Utilities.requireNonNull(question);
        view.setEnabled(false);
        controller.askOpponent(question);
    }

    /**
     * @iheritDoc
     */
    @Override
    public void quit() {
        controller.quit(this);
    }

    private void updateView(final boolean answer) throws InterruptedException {
        view.showMessage(answer ? Messages.YES : Messages.NO);
        view.updateAttempts(getPlayer().getRemainingAttempts());
        view.updateCharacters(getPlayer().getRemaining().stream().map(c -> c.getName()).collect(Collectors.toSet()));
        if (GameSettings.getDifficulty() != Difficulty.HARD) {
            view.updateQuestions(QuestionFactory.possibleQuestions(getPlayer()).stream().collect(Collectors.toSet()));
        }
    }

}
