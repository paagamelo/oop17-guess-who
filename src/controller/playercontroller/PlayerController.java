package controller.playercontroller;

import java.util.Optional;
import model.character.Character;
import model.question.Question;

/**
 * Modeling interface for a PlayerController: defines the way the Controller can interact with Player, human or cpu.
 */
public interface PlayerController {

    /**
     * Enumeration representing possible Status of the player.
     */
    enum Status {
        WON, LOST,  PLAYING, OPPONENT_QUIT;
    }

    /**
     * Getter method.
     * @throws IllegalStateException 
     *                  in case the Player has not selected yet
     * @return the Character selected by the Player
     */
    Character getSelected();

    /**
     * Getter method.
     * @return the player status.
     */
    Status getStatus();

    /**
     * Communicate the Player to start its turn.
     */
    void play();

    /**
     * Allows to ask a Question to the Player.
     * @throws InterruptedException 
     *                  in case of interruption (for instance: opponent's quit)
     * @param question 
     *                  the Question to ask
     * @return a boolean representing the answer
     */
    boolean askPlayer(Question question) throws InterruptedException;

    /**
     * Allows to register an answer.
     * @throws InterruptedException 
     *                  in case of interruption (for instance: opponent's quit).
     * @param question 
     *                  the question this Player asked for.
     * @param answer 
     *                  opponent's answer.
     */
    void registerAnswer(Question question, boolean answer) throws InterruptedException;

    /**
     * Communicate the player that game is ended.
     * @param status 
     *                  this player final status
     * @param opponentCharacter 
     *                  the opponent's Character if present
     */
    void endGame(Status status, Optional<Character> opponentCharacter);

}
