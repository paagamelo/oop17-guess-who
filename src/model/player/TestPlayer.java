package model.player;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.Test;
import controller.gameoptions.GameSettings;
import controller.gameoptions.Pack;
import controller.resources.LoadingException;
import controller.resources.Resources;

class TestPlayer {

    @Test
    public void testPlayer() {
        //assertThrows(IllegalArgumentException.class, () -> new PlayerImpl(Resources.getCharacters(), null));
        //assertThrows(IllegalArgumentException.class, () -> new PlayerImpl(null, 1));
        assertThrows(IllegalArgumentException.class, () -> new PlayerImpl(new HashSet<>(), 1));
        GameSettings.setPack(Pack.CLASSIC);
        try {
            Resources.load();
        } catch (LoadingException e) {
            e.printStackTrace();
            fail("Loading exception occurred");
        }
        final Player p = new PlayerImpl(Resources.getCharacters(), 3);
        assertThrows(IllegalStateException.class, () -> p.getSelected());
        assertThrows(IllegalArgumentException.class, () -> p.select(null));
        p.select(p.getRemaining().iterator().next());
        p.decreaseAttempts();
        System.out.println("Test Player");
        System.out.println("Selected: " + System.lineSeparator() + p.getSelected());
        System.out.println("Availables: " + System.lineSeparator() + p.getRemaining());
    }

}
