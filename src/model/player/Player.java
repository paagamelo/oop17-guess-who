package model.player;

import java.util.*;
import model.attribute.Attribute;
import model.character.Character;

/**
 * Modeling interface for a Player.
 * Players can be obtained using PlayerBuilder.
 */
public interface Player {

    /**
     * Getter method.
     * @throws IllegalStateException in case the Player has not selected yet
     * @return the Character selected by the Player
     */
    Character getSelected();

    /**
     * Getter method.
     * @return the Set of remaining possible Characters
     */
    Set<Character> getRemaining();

    /**
     * Getter method.
     * @return the number of remaining attempts to guess opponent's Character
     */
    int getRemainingAttempts();

    /**
     * Allows to select a Character, it can be called just once.
     * Note that this method should be called before calling the filter method, 
     * @throws IllegalStateException 
     *              in case the Character has already been selected
     *         IllegalArgumentException 
     *              in case of null argument or 
     *              in case the Character is not contained in the Set of available Characters
     * @param character 
     *              the Character
     */
    void select(Character character);

    /**
     * Allows to filter the possible remaining Characters through an Attribute they should have or not.
     * @throws IllegalArgumentException 
     *              in case of null argument
     *         IllegalStateException
     *              in case the Player has not selected yet (i.e. the game has not started)
     * @param attribute
     *              the attribute
     * @param b
     *              a boolean that indicates if the Characters should have or not the attribute
     */
    void filter(Attribute attribute, Boolean b);

    /**
     * Allows to decrease the number of remaining attempts.
     * @throws IllegalStateException 
     *              in case of no remaining attempts
     */
    void decreaseAttempts();

}
