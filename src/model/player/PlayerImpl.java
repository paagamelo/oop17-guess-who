package model.player;

import java.util.*;
import model.attribute.Attribute;
import model.character.Character;
import utilities.Utilities;

class PlayerImpl implements Player {

    private Character selected;
    private final Set<Character> characters;
    private int remainingAttempts;
    private boolean hasSelected;

    PlayerImpl(final Set<Character> characters, final Integer attempts) {
        Utilities.requireNonNull(characters, attempts);
        if (characters.size() < 2 || attempts < 1) {
            throw new IllegalArgumentException("Characters or attempts are less than the minimum to play");
        }
        this.characters = new HashSet<>(characters);
        this.remainingAttempts = attempts;
    }

    @Override
    public Character getSelected() {
        this.checkSelected();
        return this.selected;
    }

    @Override
    public Set<Character> getRemaining() {
        return Collections.unmodifiableSet(characters);
    }

    @Override
    public int getRemainingAttempts() {
        return this.remainingAttempts;
    }

    @Override
    public void select(final Character character) {
        if (hasSelected) {
            throw new IllegalStateException("Player has already selected");
        }
        Utilities.requireNonNull(character);
        if (!characters.contains(character)) {
            throw new IllegalArgumentException("Character is not contained in the Set of available Characters");
        }
        this.selected = character;
        hasSelected = true;
    }

    @Override
    public void filter(final Attribute attribute, final Boolean b) {
        Utilities.requireNonNull(attribute, b);
        this.checkSelected();
        characters.removeIf(character -> b ? !character.has(attribute) : character.has(attribute));
    }

    @Override
    public void decreaseAttempts() {
        if (remainingAttempts == 0) {
            throw new IllegalStateException("No remaining attempts");
        }
        this.remainingAttempts--;
    }

    private void checkSelected() {
        if (!hasSelected) {
            throw new IllegalStateException("Player has not selected yet");
        }
    }

}
