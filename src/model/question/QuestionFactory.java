package model.question;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import model.attribute.Attribute;
import model.attribute.AttributeFactory;
import model.attribute.feature.Trait;
import model.player.Player;

/**
 * Factory for Questions.
 */
public final class QuestionFactory {

    private QuestionFactory() {
    }

    /**
     * Allows to get a Question given an Attribute.
     * @throws IllegalArgumentException
     *                  in case of null argument
     * @param attribute the Attribute
     * @return the Question
     */
    public static Question from(final Attribute attribute) {
        return new QuestionImpl(attribute);
    }

    /**
     * Allows to get a Question given a Trait and a Set of Object representing its features.
     * @param <T> the most common super type of the features
     * @throws IllegalArgumentException
     *                  in case of null argument
     *                  or in case the Trait does not support the specified Features
     * @param trait the Trait
     * @param features the Features
     * @return the Question
     */
    @SafeVarargs
    public static <T> Question from(final Trait trait, final T...features) {
        return new QuestionImpl(AttributeFactory.from(trait, features));
    }

    /**
     * Allows to get all the possible questions with one or less level of detail from an Attribute,
     * i.e. one for each feature plus one about the only Trait.
     * @param attribute
     *              the Attribute
     * @return the Set of Question
     */
    public static Set<Question> possibleQuestions(final Attribute attribute) {
        return Stream.concat(Stream.of(QuestionFactory.from(attribute.getTrait())), attribute.getFeatures().stream()
                .map(feature -> QuestionFactory.from(attribute.getTrait(), feature))).collect(Collectors.toSet());
    }

    /**
     * Allows to get all the possible questions with one or less level of detail from the Set of 
     * remaining Characters of the specified Player.
     * Note that some questions may appear several times.
     * @param player
     *              the Player
     * @return a List of String, each one a possible question
     */
    public static List<Question> possibleQuestions(final Player player) {
        return player.getRemaining().stream().flatMap(character -> character.getAttributes().stream())
                .flatMap(attribute -> QuestionFactory.possibleQuestions(attribute).stream())
                .filter(question -> player.getRemaining().stream().anyMatch(c -> !c.has(question.toAttribute())))
                .collect(Collectors.toList()); //the filter is present because a question make sense only if someone does not have it
    }
}
