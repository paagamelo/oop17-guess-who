package model.character;

import java.util.Set;
import model.attribute.Attribute;

/**
 * Modeling interface for a Character, composed by a Set of Attribute.
 * In GuessWho game Characters' name is considered their identifier and has a getter method.
 * Characters can be obtained using CharacterBuilder.
 */
public interface Character {

    /**
     * Allows to get Character's name, considered unique in the game.
     * @return a String representing the name, an empty String if the Character has no name
     */
    String getName();

    /**
     * Getter method.
     * @return an unmodifiable Set of Attributes representing Character's attributes 
     */
    Set<Attribute> getAttributes();

    /**
     * Checks if the Character has a particular Attribute.
     * Note that there is a difference between this method and "getAttribute.contains()" method:
     * this method will return true if the Character has a more specific Attribute than the specified one.
     * For instance: a Character with black and curly hairs does have black hairs.
     * @throws IllegalArgumentException 
     *              in case of null argument
     * @param attribute
     *              the Attribute to seek for
     * @return a boolean 
     *              true if the Character has the attribute, false otherwise
     */
    boolean has(Attribute attribute);

}
