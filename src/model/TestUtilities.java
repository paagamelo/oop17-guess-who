package model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import model.attribute.Attribute;
import model.attribute.AttributeFactory;
import model.attribute.feature.PhysicalFeature;
import model.attribute.feature.Trait;
import model.ai.Ability;
import model.ai.Ai;
import model.character.Character;
import model.character.CharacterBuilder;

class TestUtilities {

    private static final String WRONG = "Assertion not true";

    @Test
    public void testAi() {
        assertThrows(IllegalArgumentException.class, () -> new Ai(Ability.ADVANCED).apply(null));
    }

    @Test
    public void testAttributeFactory() {
        final Attribute blackShortHairs = AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.Length.SHORT);
        assertTrue(blackShortHairs.equals(AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.Length.SHORT)), WRONG);
    }

    @Test
    public void testCharacterBuilder() {
       final CharacterBuilder cBuilder = new CharacterBuilder();
       assertThrows(IllegalStateException.class, () -> cBuilder.build());
       //assertThrows(IllegalArgumentException.class, () -> cBuilder.setName(null));
       //assertThrows(IllegalArgumentException.class, () -> cBuilder.add(null));
       cBuilder.add(AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.HairStyle.CURLY));
       cBuilder.add(AttributeFactory.from(Trait.BEARD));
       cBuilder.add(AttributeFactory.from(Trait.NAME, "test"));
       final Character c = cBuilder.build();
       assertThrows(IllegalStateException.class, () -> cBuilder.add(AttributeFactory.from(Trait.HAT)));
       assertThrows(IllegalStateException.class, () -> cBuilder.build());
       assertTrue(c.has(AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.HairStyle.CURLY)), WRONG);
       assertTrue(c.has(AttributeFactory.from(Trait.BEARD)), WRONG);
    }

}
