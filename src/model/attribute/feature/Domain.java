package model.attribute.feature;

import java.util.function.Function;

/**
 * Modeling interface for a feature Domain.
 * A Domain stores a Class<T> and a mapping function from String to <T>.
 * @param <T> the type this Domain maps to
 */
public interface Domain<T> extends Function<String, T> {

    /**
     * Allows to get Domain's name.
     * In GuessWho game Domains' names are considered unique inside a single Trait.
     * @return the name, "value" by default
     */
    String getName();

    /**
     * Allows to get the class of this Domain's type.
     * (i.e. the Class of the return type of the "apply" mapping method)
     * @return the Class of T
     */
    Class<T> getType();

}
