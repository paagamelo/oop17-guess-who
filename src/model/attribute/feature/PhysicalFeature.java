package model.attribute.feature;

/**
 * Collects all the physical features.
 */
public interface PhysicalFeature {

    /***/
    enum HairStyle {
        STRAIGHT, WAVY, CURLY, BRAIDED;
    }
    /***/
    enum Length {
        SHAVED, SHORT, LONG;
    }
    /***/
    enum Color {
        BLACK, WHITE, BROWN, BLONDE, GREEN, GINGER, GREY;
    }
    /***/
    enum Gender {
        MALE, FEMALE;
    }
    /***/
    enum EyeColor {
        BROWN, BLUE;
    }
    /***/
    enum Dimension {
        BIG, SMALL;
    }
    /***/
    enum Complexion {
        LIGHT, DARK;
    }

}
