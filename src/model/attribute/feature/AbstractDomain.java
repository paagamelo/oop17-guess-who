package model.attribute.feature;

import java.util.Objects;

/*
 * Implementation of Abstract Domain, also defines equals and hash code.
 */
abstract class AbstractDomain<T> implements Domain<T> {

    @Override
    public String getName() {
        return "value";
    }

    @Override
    public abstract Class<T> getType();

    @Override
    public abstract T apply(String t);

    @Override
    public boolean equals(final Object obj) {
        return obj instanceof Domain<?> ? ((Domain<?>) obj).getType().equals(this.getType()) : false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getType());
    }

}
