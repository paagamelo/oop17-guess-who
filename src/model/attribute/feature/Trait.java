package model.attribute.feature;

import java.util.*;
import java.util.stream.Collectors;
import utilities.ToString;

/**
 * Enumeration representing supported Traits, each one has a Set of Domains representing the possible features Domains.
 * For instance: "hairs" trait has a Set of Domains whose type is hairstyle, color, length..
 */
public enum Trait {

    /*
     * It is possible to specify directly a Domain or to specify an enumeration element, in that case
     * the Domain relative to the Enumeration type will be automatically generated.
     */ 
    /***/
    NAME(StringDomain.getDomain()), 
    /***/
    HAIRS(PhysicalFeature.Color.BLACK, PhysicalFeature.HairStyle.CURLY, PhysicalFeature.Length.SHORT),
    /***/
    @ToString(article = "a")
    BEARD(PhysicalFeature.Color.BLACK),
    /***/
    MOUSTACHE(PhysicalFeature.Color.BLACK), 
    /***/
    @ToString(article = "a")
    HAT, 
    /***/
    GLASSES, 
    /***/
    GENDER(PhysicalFeature.Gender.MALE),
    /***/
    @ToString(article = "a")
    NOSE(PhysicalFeature.Dimension.SMALL),
    /***/
    EYES(PhysicalFeature.EyeColor.BROWN),
    /***/
    @ToString(article = "a")
    MOUTH(PhysicalFeature.Dimension.SMALL),
    /***/
    COMPLEXION(PhysicalFeature.Complexion.LIGHT),
    /***/
    @ToString(article = "a")
    MASK,
    /***/
    EARRING;

    private final Set<Domain<?>> features;

    Trait() {
        this.features = new HashSet<>();
    }

    @SafeVarargs
    Trait(final Domain<?>... features) {
        this.features = new HashSet<>(Arrays.asList(features));
    }

    Trait(final Enum<?>... enums) {
        this.features = Arrays.stream(enums).map(e -> new EnumDomain<>(e)).collect(Collectors.toSet());
    }
    /**
     * @return the Set of possible features Domains
     */
    public Set<Domain<?>> getDomains() {
        return Collections.unmodifiableSet(features);
    }

    /**
     * @return a String representation of this Trait
     */
    public String toString() {
        return name().toLowerCase(Locale.ITALIAN);
    }

}
