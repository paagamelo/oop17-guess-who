package model.attribute.feature;

import java.util.Locale;
import utilities.Utilities;

/*
 * Domain implementation for Enumeration classes.
 * It stores an element of the enumeration.
 */
class EnumDomain<E extends Enum<E>> extends AbstractDomain<E> {

    private final Enum<E> element;

    EnumDomain(final Enum<E> element) {
        super();
        Utilities.requireNonNull(element);
        this.element = element;
    }

    @Override
    public String getName() {
        return element.getDeclaringClass().getSimpleName().toLowerCase(Locale.ITALIAN);
    }

    @Override
    public Class<E> getType() {
        return element.getDeclaringClass();
    }

    @Override
    public E apply(final String t) {
        Utilities.requireNonNull(t);
        return Enum.valueOf(getType(), t.toUpperCase(Locale.ITALIAN));
    }

}
