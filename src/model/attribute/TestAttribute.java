package model.attribute;

import static model.attribute.feature.Trait.HAIRS;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import model.attribute.feature.PhysicalFeature;

class TestAttribute {

    private static final String WRONG = "Assertion was false";

    @Test
    public void testAttribute() {
        //assertThrows(IllegalArgumentException.class, () -> new AttributeImpl(null, null));
        final Attribute rufyHairs = AttributeFactory.from(HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.Length.SHORT);
        assertTrue(rufyHairs.getTrait().equals(HAIRS), WRONG);
        assertTrue(rufyHairs.getFeatures().contains(PhysicalFeature.Color.BLACK), WRONG);
        assertTrue(rufyHairs.getFeatures().contains(PhysicalFeature.Length.SHORT), WRONG);
        assertFalse(rufyHairs.getFeatures().contains(PhysicalFeature.HairStyle.CURLY), "Assertion was true");
        final Attribute rufyHairs2 = AttributeFactory.from(HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.Length.SHORT);
        assertTrue(rufyHairs.equals(rufyHairs2), WRONG);
    }

}
