package model.attribute;

import java.util.*;

import model.attribute.feature.Trait;

/**
 * Factory for Attributes.
 * Note that there is no way to include features that do not belong to the Trait you are describing or
 * to include multiple features belonging to the same domain, in that case an IllegalArgumentException will be thrown.
 */
public final class AttributeFactory {

    private AttributeFactory() {
    }

    /**
     * Allows to build an Attribute, given a Trait and a Set of Objects (its features).
     * @param trait 
     *              the trait the Attribute will represent
     * @param features
     *              the features of the Attribute
     * @return the Attribute 
     */
    public static Attribute from(final Trait trait, final Set<?> features) {
        return new AttributeImpl(trait, features);
    }

    /**
     * Allows to build an Attribute, given a Trait and a variable number of features.
     * @param <T> 
     *              the most common super type of the features
     * @param trait 
     *              the trait the Attribute will represent
     * @param features
     *              the features of the Attribute
     * @return the Attribute 
     */
    @SafeVarargs
    public static <T> Attribute from(final Trait trait, final T... features) {
        return AttributeFactory.from(trait, new HashSet<>(Arrays.asList(features)));
    }

}
